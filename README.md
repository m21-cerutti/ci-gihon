# CI-Gihon

Watchout, it depends on the forked godot project https://gitlab.com/m21-cerutti/godot-engine, and not the official one.
The CI and all the validation are done on a private instance, the repository serve for collaboration and to gather feedbacks and issues.

## License
Copyright (c) 2023 Marc Cerutti
